import unittest
from astropy.io import fits
from create_cutout import cut_around

class TestCutout (unittest.TestCase):
    def test_coor(self):
        '''
         Testing coordinate imputs
        '''

        RA= ( 0. +59.0/60.0  +49.91/3600.0) * 15.0
        DEC= -(7. +34.0/60.0 +44.64/3600.0)
        self.assertTrue(0.0 <= RA <= 360.0)
        self.assertTrue(-90.0 <= DEC <=+90.0)

    def test_frec(self):
        '''
         Testing frecuency input
        '''
        VLASS_FITS = 'VLASS1.1.ql.T09t02.J005808-073000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'
        hdulist = fits.open(VLASS_FITS)
        frec=hdulist[0].header['CRVAL3']
        self.assertTrue(2.0e9 <= float(frec) <= 4.0e9)
        hdulist.close(VLASS_FITS)

if __name__ == '__main__':
     unittest.main()



