# Brian R. Kent, NRAO
# NINE University of West Indies Workshop
# Loading and displaying a FITS image

# IMPORTS ----------------------------------
from astropy.io import fits
from astropy import wcs

import numpy as np
import matplotlib.pyplot as plt



def cut_around(RA,DEC,HALF_Width):
    '''
       This function reads the right ascension and declination of an image and make a cutout
       of the size given by the user

    '''
    #1 open VLASS File. This contains HEADER and DATA
    hdulist = fits.open(VLASS_FITS)    
    #2 Use the VLASS FITS header data to create a world coordinate system(wcs)
    w = wcs.WCS(hdulist[0].header)
    #3 get all the raw data from the VLASS file
    VLASS_image_data = fits.getdata(VLASS_FITS)[0,0,:,:] 
    #4 Put the coordinates into an Array for AstroPy
    world = np.array([[RA, DEC, 0.0, 0.0]], np.float_)
    #5 Use the WCS to convert from RA/DEC to pixel locations on the image
    Pixel_Coord = w.wcs_world2pix(world,1)
    x = int(round(Pixel_Coord[0][1]))
    y = int(round(Pixel_Coord[0][0]))
    #6 create 4 corners of cutout
    lower_x = x - HALF_Width
    upper_x = x + HALF_Width

    lower_y = y - HALF_Width
    upper_y = y + HALF_Width
    #7 Use the pixel coordinates to make cutout

    return VLASS_image_data[lower_x:upper_x,lower_y:upper_y]

# VARIABLES -------------------------------
RA =   ( 0. +59.0/60.0  +49.91/3600.0) * 15.0
DEC = -(7. +34.0/60.0 +44.64/3600.0)
#RA = (1.0+11.0/60.0+09.045/3600.0) * 15.0
#DEC = -(13.0+57/60.0+38.96/3600.0)
HALF_Width = 92   #(92 pixels is approx. 1.5 arcminutes)
VLASS_FITS = 'VLASS1.1.ql.T09t02.J005808-073000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'
#VLASS_FITS = 'VLASS1.1.ql.T07t02.J011047-133000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'


vlasscutout=cut_around(RA,DEC,HALF_Width)

## Here i added these 3 lines of command to create a cuttout of the vlass big image, this contains only the object image centered in its coordinates

hdulist = fits.open(VLASS_FITS)
obs = hdulist[0].data
obs_head = hdulist[0].header
newheader=obs_head.copy()
newheader[38]=RA
newheader[40]=vlasscutout.shape[0]/2.0

newheader[43]=DEC
newheader[45]=vlasscutout.shape[1]/2.0

    

outfile='VLASS_cutout.fits'
hdu_s=fits.PrimaryHDU(vlasscutout, header=newheader)
hdu_s.writeto(outfile)



'''
#8 Plot onto graph with Matplotlib
plt.imshow(vlasscutout, origin='lower', cmap='inferno')
plt.colorbar()
plt.show()

'''
